const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");
const validator = require("validator");
const bcrypt = require('bcryptjs')

//Define your schema
const memberSchema = new Schema(
	{
		firstName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: "J"
		},

		lastName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: "Doe"
		},
		username: {
			type: String,
			required: true,
			trim: true,
			maxlength: 10,
			index: true,
			unique: true,
			validate(value) {
				if(!validator.isAlphanumeric(value)) {
					throw new Error("No special characters!")
				}
				console.log("Username has been added")
			}
		},
		position: {
			type: String,
			enum: ["instructor", "student", "hr", "admin", "ca" ],
			required: true
		},
		age: {
			type: Number,
			min: 18,
			required: true
		},
		email:
		{
			type: String,
			required: true,
			trim: true,
			unique: true,
			validate(value) {
				if(!validator.isEmail(value)) {
					throw new Error("Email is invalid!")
				}
			}
		},
		password:
		{
			type: String,
			required: true,
			minlength: [5, "Password must have at least 5 characters!"]
		}
		//tokens
	},
	{
		timestamps: true
	}

);

//HASH PASSWORD WHEN UPDATING AND CREATING MEMBER
memberSchema.pre("save", async function(next) { 
	//we will use function because arrow function ()=> does not bind this
	const member = this

	if(member.isModified('password')) {
		//salt
		const salt = await bcrypt.genSalt(10);
		//hash
		member.password = await bcrypt.hash(member.password, salt);
	}

	next();
})

//Apply the uniqueValidator plugin to userSchema
memberSchema.plugin(uniqueValidator);

//Export the model
module.exports = mongoose.model("Member", memberSchema);

memberSchema.plugin(uniqueValidator);

//Export your model
module.exports = mongoose.model("Member", memberSchema);
