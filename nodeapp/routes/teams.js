//Declar dependencies and model
const Team = require("../models/teams")
const express = require("express")
const router = express.Router() //to handle routing


//Create Routes/Endpoints
//1.) Create
//a.) teams

router.post("/", async (req, res) => {
	// console.log("test");
	// return res.send(req.body);
	const team = new Team(req.body);
	//save to database
	// team.save()
	// 	.then(() => {
	// 		res.send(team)
	// 	})
	// 	.catch((e) => {
	// 		//BAD REQUEST (http response status codes)
	// 		res.status(400).send(e)
	// 	})
	try {
		await team.save()
		res.send(team)
	} catch(e) {
		res.status(400).send(e)
	}
})

//GET ALL
router.get("/", async (req, res) => {
	// return res.send("get all teams");
	// Team.find()
	// 	.then((teams) => {
	// 		return res.status(200).send(teams)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	try {
		const teams = await Team.find()
		res.status(200).send(teams)
		console.log("no return")
	} catch(e) {
		return res.status(404).send(e)
		console.log("after return 2")
	}
})

router.get("/:id", async (req, res) => {
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id

	//Mongoose Models Query
	// Team.findById(_id)
	// 	.then((team) =>{
	// 		if(!team) {
	// 			return res.status(404).send(e)
	// 		}
	// 		return res.send(team)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})

	try {
		const team = await Team.findById(_id)
		//if team is {}
		if(!team) {
			//not yet working
			return res.status(404).send(e)
		}
		res.send(team)

	} catch(e) {
		console.log("error 500")
		return res.send(e)
	}
})

router.patch("/:id", async (req, res) => {
	// return res.send("update a team");
	const _id = req.params.id

	//Mongoose Models Query
	// Team.findByIdAndUpdate(_id, req.body, { new:true })
	// 	.then((team) => {
	// 		if(!team) {
	// 			//NOT FOUND
	// 			return res.status(404).send(e)
	// 		}
	// 		return res.send(team)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	try {
		const team = await Team.findByIdAndUpdate(_id, req.body, {new: true})
		if(!team) {
			return res.status(404).send(e)
		}
		res.send(team)
	} catch(e) {
		return res.status(500).send(e)
	}
})

router.delete("/:id", async (req, res) => {
	// return res.send("delete a team");
	const _id = req.params.id

	//Mongoose Models Query
	Team.findByIdAndDelete(_id)
		.then((team) => {
			if(!team) {
				return res.send(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
	try {
		const team = await Team.findByIdAndDelete(_id)
		if(!team) {
			return res.status(404).send("Team doesn't exist")
		}
		res.send(team)
	} catch(e) {
		res.status(500).send(e.message)
	}
})

module.exports = router